package uk.co.harieo.Honour.utils;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import uk.co.harieo.Honour.Honour;

import java.util.UUID;

public class HonourAPI {

    /**
     * Gets a Player's honour directly from live storage
     *
     * @param player to retrieve honour for
     * @return the amount of honour they have or default if it cannot be found
     */
    public static int getHonour(Player player) {
        FileConfiguration log = Honour.getPlayerLog();
        if(!log.contains(player.getUniqueId().toString() + ".honour")) {
            return 100; // Default honour as a placeholder
        }
        return log.getInt(player.getUniqueId().toString() + ".honour");
    }

    /**
     * Gets a Player's honour directly from live storage using
     * a UUID
     *
     * @param uuid to retrieve honour for
     * @return the amount of honour they have or default if it cannot be found
     */
    public static int getHonour(UUID uuid) {
        FileConfiguration log = Honour.getPlayerLog();
        if(!log.contains(uuid.toString() + ".honour")) {
            return 100; // Default honour as a placeholder
        }
        return log.getInt(uuid.toString() + ".honour");
    }

    /**
     * Adds the specified amount of honour to a Player's
     * honour, immediately stored
     *
     * @param player to add honour for
     * @param amount to add to current amount of honour
     * @return whether the change was successful
     */
    public static boolean addHonour(Player player, int amount) {
        FileConfiguration log = Honour.getPlayerLog();
        if(!log.contains(player.getUniqueId().toString() + ".honour")) {
            return false; // Prevents attempting to edit a log that does not exist
        }
        log.set(player.getUniqueId().toString() + ".honour", getHonour(player) + amount);
        Bukkit.getPluginManager().callEvent(new HonourUpdateEvent(player.getUniqueId(), amount));
        return true;
    }

    /**
     * Adds the specified amount of honour to a Player's
     * honour, immediately stored
     *
     * @param uuid to add honour for
     * @param amount to add to current amount of honour
     * @return whether the change was successful
     */
    public static boolean addHonour(UUID uuid, int amount) {
        FileConfiguration log = Honour.getPlayerLog();
        if(!log.contains(uuid.toString() + ".honour")) {
            return false; // Prevents attempting to edit a log that does not exist
        }
        log.set(uuid.toString() + ".honour", getHonour(uuid) + amount);
        Bukkit.getPluginManager().callEvent(new HonourUpdateEvent(uuid, amount));
        return true;
    }

    /**
     * Reduces the specified amount of honour from a Player's
     * honour, immediately stored
     *
     * @param player to remove honour from
     * @param amount to remove from the current amount of honour
     * @return whether the change was successful
     */
    public static boolean deductHonour(Player player, int amount) {
        FileConfiguration log = Honour.getPlayerLog();
        if(!log.contains(player.getUniqueId().toString() + ".honour")) {
            return false; // Prevents attempting to edit a log that does not exist
        }
        log.set(player.getUniqueId().toString() + ".honour", getHonour(player) - amount);
        Bukkit.getPluginManager().callEvent(new HonourUpdateEvent(player.getUniqueId(), -amount));
        return true;
    }

    /**
     * Reduces the specified amount of honour from a Player's
     * honour, immediately stored
     *
     * @param uuid to remove honour from
     * @param amount to remove from the current amount of honour
     * @return whether the change was successful
     */
    public static boolean deductHonour(UUID uuid, int amount) {
        FileConfiguration log = Honour.getPlayerLog();
        if(!log.contains(uuid.toString() + ".honour")) {
            return false; // Prevents attempting to edit a log that does not exist
        }
        log.set(uuid.toString() + ".honour", getHonour(uuid) - amount);
        Bukkit.getPluginManager().callEvent(new HonourUpdateEvent(uuid, amount));
        return true;
    }

    /**
     * Sets a Player's honour to a specified value
     *
     * @param player to set honour for
     * @param amount to set honour to
     * @return whether the change was successful
     */
    public static boolean setHonour(Player player, int amount) {
        FileConfiguration log = Honour.getPlayerLog();
        if(!log.contains(player.getUniqueId().toString() + ".honour")) {
            return false; // Prevents attempting to edit a log that does not exist
        }
        log.set(player.getUniqueId().toString() + ".honour", amount);
        Bukkit.getPluginManager().callEvent(new HonourUpdateEvent(player.getUniqueId(), amount));
        return true;
    }

    /**
     * Sets a Player's honour to a specified value
     * from a UUID
     *
     * @param uuid to set honour for
     * @param amount to set honour to
     * @return whether the change was successful
     */
    public static boolean setHonour(UUID uuid, int amount) {
        FileConfiguration log = Honour.getPlayerLog();
        if(!log.contains(uuid.toString() + ".honour")) {
            return false; // Prevents attempting to edit a log that does not exist
        }
        log.set(uuid.toString() + ".honour", amount);
        Bukkit.getPluginManager().callEvent(new HonourUpdateEvent(uuid, amount));
        return true;
    }

    /**
     * Checks whether the Honour plugin has data stored
     * for a specified Player
     *
     * @param player to check against the storage system
     * @return whether the player has a stored log
     */
    public static boolean isPlayerLogged(Player player) {
        FileConfiguration log = Honour.getPlayerLog();
        return log.contains(player.getUniqueId().toString() + ".honour");
    }

    /**
     * Checks whether the Honour plugin has data stored
     * for a specified UUID
     *
     * @param uuid to check against the storage system
     * @return whether the UUID has a stored log
     */
    public static boolean isPlayerLogged(UUID uuid) {
        FileConfiguration log = Honour.getPlayerLog();
        return log.contains(uuid.toString() + ".honour");
    }

    /**
     * Creates data for a Player if no data already exists
     *
     * @param player to add to storage
     */
    public static void logPlayer(Player player) {
        FileConfiguration log = Honour.getPlayerLog();
        if(!log.contains(player.getUniqueId().toString() + ".honour")) {
            log.createSection(player.getUniqueId().toString());
            log.set(player.getUniqueId().toString() + ".honour", 100);
        }
    }

}
