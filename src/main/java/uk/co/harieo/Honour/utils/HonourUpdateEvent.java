package uk.co.harieo.Honour.utils;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.UUID;

public class HonourUpdateEvent extends Event {

    private UUID uuid;
    private int amount;

    public HonourUpdateEvent(UUID uuid, int change) {
        this.uuid = uuid;
        this.amount = change;
    }

    /**
     * Get the UUID of the Player in question
     *
     * @return the UUID of the Player being affected
     */
    public UUID getUniqueId() {
        return uuid;
    }

    /**
     * Get the amount of Honour changed
     * by this event
     *
     * @return the amount of Honour lost, gained or set
     */
    public int getChange() {
        return amount;
    }


    private static final HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
