package uk.co.harieo.Honour.listeners.buffs;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import uk.co.harieo.Honour.Honour;
import uk.co.harieo.Honour.enums.Buffs;
import uk.co.harieo.Honour.utils.HonourAPI;

import java.util.Random;

public class Agile implements Listener {

    @EventHandler
    public void onEntityDamage(EntityDamageByEntityEvent event) {

        if(event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            Player player = (Player) event.getEntity();
            Player damager = (Player) event.getDamager();
            Random random = new Random();

            if(Buffs.AGILE3.hasSufficientHonour(player)) {
                if(random.nextInt(100) <= 25) {
                    event.setCancelled(true);
                    player.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You quickly roll to the side, " + ChatColor.GREEN + "dodging "
                            + ChatColor.GRAY + "the incoming attack");
                    damager.sendMessage(Honour.PREFIX + ChatColor.GRAY + "Your enemy makes an expert " + ChatColor.RED + "dodging "
                            + ChatColor.GRAY + "manoeuvre");
                }
            } else if(Buffs.AGILE2.hasSufficientHonour(player)) {
                if(random.nextInt(100) <= 15) {
                    event.setCancelled(true);
                    player.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You quickly roll to the side, " + ChatColor.GREEN + "dodging "
                            + ChatColor.GRAY + "the incoming attack");
                    damager.sendMessage(Honour.PREFIX + ChatColor.GRAY + "Your enemy makes an expert " + ChatColor.RED + "dodging "
                            + ChatColor.GRAY + "manoeuvre");
                }
            } else if(Buffs.AGILE.hasSufficientHonour(player)) {
                if(random.nextInt(100) <= 5) {
                    event.setCancelled(true);
                    player.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You quickly roll to the side, " + ChatColor.GREEN + "dodging "
                            + ChatColor.GRAY + "the incoming attack");
                    damager.sendMessage(Honour.PREFIX + ChatColor.GRAY + "Your enemy makes an expert " + ChatColor.RED + "dodging "
                            + ChatColor.GRAY + "manoeuvre");
                }
            }
        }

    }

}
