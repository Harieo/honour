package uk.co.harieo.Honour.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import uk.co.harieo.Honour.Honour;
import uk.co.harieo.Honour.utils.HonourAPI;

public class GainHonour implements Listener {

    // Damage and Death related events are located in CombatTracker for easier compatibility //

    @EventHandler
    public void onPotionSplash(PotionSplashEvent event) {

        ThrownPotion potion = event.getPotion();
        boolean proceed = false;
        for(PotionEffect p : potion.getEffects()) {
            if(p.getType().equals(PotionEffectType.HEAL) || p.getType().equals(PotionEffectType.REGENERATION)) {
                proceed = true;
            }
        }
        if(!proceed) {
            return;
        } else if (!(potion.getShooter() instanceof Player)) {
            return;
        }

        Player player = (Player) potion.getShooter();

        player.sendMessage(Honour.PREFIX + ChatColor.GREEN + "You've gained (+50) Honour for healing an ally");
        HonourAPI.addHonour(player, 50);

    }

}
