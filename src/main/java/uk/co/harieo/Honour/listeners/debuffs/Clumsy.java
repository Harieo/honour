package uk.co.harieo.Honour.listeners.debuffs;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import uk.co.harieo.Honour.Honour;
import uk.co.harieo.Honour.enums.Buffs;
import uk.co.harieo.Honour.enums.Debuffs;

import java.util.Random;

public class Clumsy {

    public static void checkClumsiness(EntityDamageByEntityEvent event) {

        if(!Honour.allowDebuffs()) {
            return;
        }

        if(event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            Player player = (Player) event.getEntity();
            Player damager = (Player) event.getDamager();
            Random random = new Random();

            if (damager.hasPermission("honour.immune")) {
                return;
            }

            if(Debuffs.CLUMSY2.hasSufficientHonour(damager)) {
                if(random.nextInt(100) <= 25) {
                    damager.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20*5, 1));
                    damager.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You trip over your own feet, giving you "
                            + ChatColor.YELLOW + "Slowness " + ChatColor.GRAY + "while you recover");
                    player.sendMessage(Honour.PREFIX + ChatColor.GRAY + "Your enemy clumsily trips over their own feet, giving them "
                            + ChatColor.YELLOW + "Slowness");
                }
            } else if(Debuffs.CLUMSY.hasSufficientHonour(damager)) {
                if(random.nextInt(100) <= 10) {
                    damager.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20*5, 1));
                    damager.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You trip over your own feet, giving you "
                            + ChatColor.YELLOW + "Slowness " + ChatColor.GRAY + "while you recover");
                    player.sendMessage(Honour.PREFIX + ChatColor.GRAY + "Your enemy clumsily trips over their own feet, giving them "
                            + ChatColor.YELLOW + "Slowness");
                }
            }
        }

    }

}
