package uk.co.harieo.Honour.listeners.buffs;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import uk.co.harieo.Honour.Honour;
import uk.co.harieo.Honour.enums.Buffs;

import java.util.ArrayList;
import java.util.List;

public class GloriousVictory implements Listener {

    // Stores a list of evil murderers to prevent double initiation
    private static List<Player> buffer = new ArrayList<>();

    @EventHandler
    public void onEntityDamage(EntityDamageByEntityEvent event) {
        if(Honour.getMode() != Honour.Mode.WAR_TORN) {
            return;
        }

        if(event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            Player player = (Player) event.getEntity();
            Player damager = (Player) event.getDamager();

            if(!buffer.contains(damager)) {
                if(player.getHealth() <= event.getDamage()) {
                    buffer.add(damager);
                    if(Buffs.GLORIOUS_VICTORY.hasSufficientHonour(damager)) {
                        damager.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 10 * 20, 0));
                        damager.sendMessage(Honour.PREFIX + ChatColor.GOLD + "Expert kill! " + ChatColor.GRAY + "Your satisfaction begins "
                                + ChatColor.GREEN + "Regenerating " + ChatColor.GRAY + "you for " + ChatColor.YELLOW + "10 seconds "
                                + ChatColor.GRAY + "(game logic)");
                    }
                }
            }
        }

    }

}
