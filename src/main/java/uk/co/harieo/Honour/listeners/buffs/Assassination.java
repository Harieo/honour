package uk.co.harieo.Honour.listeners.buffs;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import uk.co.harieo.Honour.Honour;
import uk.co.harieo.Honour.enums.Buffs;

import java.util.Random;

public class Assassination {

    public static void checkAssassination(EntityDamageByEntityEvent event) {

        if(Honour.getMode() != Honour.Mode.WAR_TORN) {
            return;
        } else if(!Honour.allowBuffs()) {
            return;
        }

        if(event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            Player player = (Player) event.getEntity();
            Player damager = (Player) event.getDamager();
            Random random = new Random();

            if(Buffs.ASSASSINATION2.hasSufficientHonour(damager)) {
                if(random.nextInt(100) <= 25) {
                    player.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 5*20, 0));
                    player.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You were caught off-guard and you stumble, making you "
                            + ChatColor.RED + "Weaker " + ChatColor.GRAY + "for " + ChatColor.YELLOW + "5 seconds");
                    damager.sendMessage(Honour.PREFIX + ChatColor.GRAY + "Your opponent makes a foolish error giving them temporary "
                            + ChatColor.YELLOW + "Weakness");
                }
            } else if(Buffs.ASSASSINATION.hasSufficientHonour(damager)) {
                if(random.nextInt(100) <= 10) {
                    player.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 5*20, 0));
                    player.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You were caught off-guard and you stumble, making you "
                            + ChatColor.RED + "Weaker " + ChatColor.GRAY + "for " + ChatColor.YELLOW + "5 seconds");
                    damager.sendMessage(Honour.PREFIX + ChatColor.GRAY + "Your opponent makes a foolish error giving them temporary "
                            + ChatColor.YELLOW + "Weakness");
                }
            }
        }

    }

}
