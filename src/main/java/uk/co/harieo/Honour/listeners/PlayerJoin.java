package uk.co.harieo.Honour.listeners;

import org.bukkit.BanList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import uk.co.harieo.Honour.Honour;

public class PlayerJoin implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {

        FileConfiguration log = Honour.getPlayerLog();
        Player player = event.getPlayer();
        int honour = 100;

        // Adds the player to the log if they are not already there
        if(!log.contains(player.getUniqueId().toString())) {
            log.createSection(player.getUniqueId().toString());
            log.set(player.getUniqueId().toString() + ".honour", 100); // 100 starting Honour for all players
            Honour.getInstance().getLogger().info("Registered " + player.getName() + " in Honour plugin!");
        } else {
            honour = log.getInt(player.getUniqueId().toString() + ".honour");
        }

        if(honour <= Honour.getBanHonour() && !player.hasPermission("honour.immune")) {
            String kickMessage = "        " + Honour.PREFIX + ChatColor.RED + "You have been Permanently Banned! \n"
                    + ChatColor.GRAY + "    You were banned for falling below the minimum Honour limit automatically. \n"
                    + "\n"
                    + ChatColor.YELLOW + "    This server uses the " + ChatColor.AQUA + ChatColor.BOLD.toString() + "Honour "
                    + ChatColor.YELLOW + "moderation plugin";

            String banMessage = ChatColor.GRAY + "You were banned for falling below the minimum Honour limit automatically.\n"
                    + ChatColor.YELLOW + "  This server uses the " + ChatColor.AQUA + ChatColor.BOLD.toString() + "Honour "
                    + ChatColor.YELLOW + "moderation plugin";

            Bukkit.getBanList(BanList.Type.NAME).addBan(player.getName(), banMessage, null, "Honour Moderation Plugin");
            player.kickPlayer(kickMessage);
        }

        // Sends join messages if applicable
        if(Honour.allowJoinMessage()) {
            if(honour >= 10000) {
                Bukkit.broadcastMessage(Honour.PREFIX + ChatColor.GRAY + "Welcome the "
                        + ChatColor.GREEN + ChatColor.BOLD.toString() + "Honourable " + ChatColor.GRAY + "player "
                        + ChatColor.YELLOW + player.getName()
                        + ChatColor.GRAY + " back to the server!");
                player.sendMessage(""); // Spacer for the welcoming message
            } else if (honour <= -1000) {
                Bukkit.broadcastMessage(Honour.PREFIX + ChatColor.GRAY + "Beware of the "
                        + ChatColor.RED + ChatColor.BOLD.toString() + "Disgraced " + ChatColor.GRAY + "player "
                        + ChatColor.YELLOW + player.getName()
                        + ChatColor.GRAY + " as they have little Honour left...");
                player.sendMessage(""); // Spacer for the welcoming message
            }
        }

        player.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You currently have " + ChatColor.YELLOW + honour + " Honour "
                + ChatColor.GRAY + "on this server");
        player.sendMessage(Honour.PREFIX + ChatColor.GRAY + "Use the command " + ChatColor.YELLOW + "/honour info "
                + ChatColor.GRAY + "to see details of your buffs/debuffs");

    }

}
