package uk.co.harieo.Honour.listeners.debuffs;

import org.bukkit.BanList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import uk.co.harieo.Honour.Honour;
import uk.co.harieo.Honour.utils.HonourAPI;
import uk.co.harieo.Honour.utils.HonourUpdateEvent;

public class AutoBan implements Listener {

    @EventHandler
    public void onHonourChange(HonourUpdateEvent event) {

        if (!Honour.allowAutoBan()) {
            return;
        } else if (HonourAPI.getHonour(event.getUniqueId()) > Honour.getBanHonour()) {
            return;
        }

        Player player = Bukkit.getPlayer(event.getUniqueId());
        if (player == null) {
            return;
        } else if (player.hasPermission("honour.immune")) {
            return;
        }

        String kickMessage = "        " + Honour.PREFIX + ChatColor.RED + "You have been Permanently Banned! \n"
                + ChatColor.GRAY + "    You were banned for falling below the minimum Honour limit automatically. \n"
                + "\n"
                + ChatColor.YELLOW + "    This server uses the " + ChatColor.AQUA + ChatColor.BOLD.toString() + "Honour "
                + ChatColor.YELLOW + "moderation plugin";

        String banMessage = ChatColor.GRAY + "You were banned for falling below the minimum Honour limit automatically.\n"
                + ChatColor.YELLOW + "  This server uses the " + ChatColor.AQUA + ChatColor.BOLD.toString() + "Honour "
                + ChatColor.YELLOW + "moderation plugin";

        Bukkit.getBanList(BanList.Type.NAME).addBan(player.getName(), banMessage, null, "Honour Moderation Plugin");
        player.kickPlayer(kickMessage);

    }

}
