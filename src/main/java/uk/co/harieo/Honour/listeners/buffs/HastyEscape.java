package uk.co.harieo.Honour.listeners.buffs;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import uk.co.harieo.Honour.Honour;
import uk.co.harieo.Honour.enums.Buffs;
import uk.co.harieo.Honour.listeners.CombatTracker;

public class HastyEscape implements Listener {

    @EventHandler (priority = EventPriority.NORMAL)
    public void onEntityDamage(EntityDamageByEntityEvent event) {

        if(Honour.getMode() != Honour.Mode.PEACEFUL) {
            return;
        }

        if(event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            Player player = (Player) event.getEntity();
            Player damager = (Player) event.getDamager();

            if(!Buffs.HASTY_ESCAPE.hasSufficientHonour(player)) {
                return;
            }

            boolean proceed = false;
            for(PotionEffect p : player.getActivePotionEffects()) {
                if(p.getType() == PotionEffectType.SPEED) {
                    proceed = true;
                }
            }

            if(proceed) {
                if (CombatTracker.isTarget(player, damager)) {
                    if (player.getHealth() - event.getDamage() <= 8.0) {
                        player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 20, 1));
                        player.sendMessage(Honour.PREFIX + ChatColor.GRAY + "Your guardian server plugin grants you "
                                + ChatColor.AQUA + "Speed II " + ChatColor.GRAY + "to make a " + ChatColor.YELLOW + "swift retreat");
                    }
                }
            }
        }

    }

}
