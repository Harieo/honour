package uk.co.harieo.Honour.listeners.buffs;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import uk.co.harieo.Honour.Honour;
import uk.co.harieo.Honour.enums.Buffs;

import java.util.Random;

public class DivineProtection {

    public static void checkDivineProtection(EntityDamageByEntityEvent event) {

        if(Honour.getMode() != Honour.Mode.PEACEFUL) {
            return;
        } else if(!Honour.allowBuffs()) {
            return;
        }

        if(event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            Player player = (Player) event.getEntity();
            Player damager = (Player) event.getDamager();
            Random random = new Random();

            if(Buffs.DIVINE_PROTECTION2.hasSufficientHonour(player)) {
                if(random.nextInt(100) <= 25) {
                    event.setCancelled(true);
                    damager.damage(event.getDamage());
                    damager.sendMessage(Honour.PREFIX + ChatColor.RED + "Your enemy has a divine shield which deflected your damage!");
                    player.sendMessage(Honour.PREFIX + ChatColor.GRAY + "Your guardian " + ChatColor.GREEN + "deflects "
                            + ChatColor.GRAY + "the damage but they will not do it again...");
                }
            } else if(Buffs.DIVINE_PROTECTION.hasSufficientHonour(player)) {
                if(random.nextInt(100) <= 10) {
                    event.setCancelled(true);
                    damager.damage(event.getDamage());
                    damager.sendMessage(Honour.PREFIX + ChatColor.RED + "Your enemy has a divine shield which deflected your damage!");
                    player.sendMessage(Honour.PREFIX + ChatColor.GRAY + "Your guardian " + ChatColor.GREEN + "deflects "
                            + ChatColor.GRAY + "the damage but they will not do it again...");
                }
            }
        } else if (event.getEntity() instanceof Player && event.getDamager() instanceof Projectile) {
            Projectile projectile = (Projectile) event.getDamager();
            if(projectile.getShooter() instanceof Player) {
                Player player = (Player) event.getEntity();
                Player damager = (Player) projectile.getShooter();
                Random random = new Random();

                if(Buffs.DIVINE_PROTECTION2.hasSufficientHonour(player)) {
                    if(random.nextInt(100) <= 25) {
                        event.setCancelled(true);
                        damager.damage(event.getDamage());
                        damager.sendMessage(Honour.PREFIX + ChatColor.RED + "Your enemy has a divine shield which deflected your damage!");
                        player.sendMessage(Honour.PREFIX + ChatColor.GRAY + "Your guardian " + ChatColor.GREEN + "deflects "
                                + ChatColor.GRAY + "the damage but they will not do it again...");
                    }
                } else if(Buffs.DIVINE_PROTECTION.hasSufficientHonour(player)) {
                    if(random.nextInt(100) <= 10) {
                        event.setCancelled(true);
                        damager.damage(event.getDamage());
                        damager.sendMessage(Honour.PREFIX + ChatColor.RED + "Your enemy has a divine shield which deflected your damage!");
                        player.sendMessage(Honour.PREFIX + ChatColor.GRAY + "Your guardian " + ChatColor.GREEN + "deflects "
                                + ChatColor.GRAY + "the damage but they will not do it again...");
                    }
                }
            }
        }

    }

}
