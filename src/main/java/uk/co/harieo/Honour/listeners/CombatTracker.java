package uk.co.harieo.Honour.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import uk.co.harieo.Honour.Honour;
import uk.co.harieo.Honour.listeners.buffs.Assassination;
import uk.co.harieo.Honour.listeners.buffs.DivineProtection;
import uk.co.harieo.Honour.listeners.debuffs.Clumsy;
import uk.co.harieo.Honour.utils.HonourAPI;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class CombatTracker implements Listener {

    // Map formatted as <Attacker, Target>
    private static HashMap<Player, Player> cooldown = new HashMap<>();
    // Map formatted as <Attacker, Targets> //
    private static HashMap<Player, List<Player>> tracking = new HashMap<>();
    // Used to store dying players to prevent double trigger //
    private static List<Player> buffer = new ArrayList<>();

    /**
     * Marks two players as in combat to be applied
     * within the CombatLogger
     *
     * @param attacker who started the fight
     * @param target who is under attack
     */
    public static void enterCombat(Player attacker, Player target) {
        if(cooldown.containsKey(attacker) && cooldown.get(attacker).equals(target)) {
            return;
        } else if (cooldown.containsKey(target) && cooldown.get(target).equals(attacker)) {
            return;
        } else if (tracking.containsKey(attacker) && tracking.get(attacker).contains(target)) {
            return;
        } else if (tracking.containsKey(target) && tracking.get(target).contains(attacker)) {
            return;
        } else if (tracking.containsKey(attacker)) {
            tracking.get(attacker).add(target);

            if(Honour.getMode() == Honour.Mode.WAR_TORN) {
                attacker.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You have entered into combat with "
                        + ChatColor.GOLD + target.getName());
                attacker.sendMessage(Honour.PREFIX + ChatColor.GREEN + "You have gained (+50) Honour for aggression");
                HonourAPI.addHonour(attacker, 50);
                target.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You are now in combat with "
                        + ChatColor.GOLD + attacker.getName());
            } else {
                attacker.sendMessage(Honour.PREFIX + ChatColor.YELLOW + "You have entered into combat with "
                        + ChatColor.RED + target.getName());
                attacker.sendMessage(Honour.PREFIX + ChatColor.RED + "You have lost (-50) Honour for starting " +
                        "an unnecessary fight");
                HonourAPI.deductHonour(attacker, 50);
                target.sendMessage(Honour.PREFIX + ChatColor.YELLOW + "You are now in combat with "
                        + ChatColor.RED + attacker.getName());
                target.sendMessage(Honour.PREFIX + ChatColor.GREEN + "You will not lose Honour for defending yourself!");
            }

            BukkitRunnable runnable = new BukkitRunnable() {
                @Override
                public void run() {
                    cancel();
                    if(tracking.containsKey(attacker) && tracking.get(attacker).contains(target)) {
                        if(Honour.getMode() == Honour.Mode.PEACEFUL) {
                            attacker.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You have " + ChatColor.RED + "left combat, "
                                    + ChatColor.GRAY + "attacking " + ChatColor.YELLOW + target.getName() + ChatColor.GRAY
                                    + " again will make you lose further Honour");
                            target.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You have " + ChatColor.RED + "left combat, "
                                    + ChatColor.GRAY + "you will " + ChatColor.RED + ChatColor.BOLD.toString() + "lose karma "
                                    + ChatColor.GRAY + "if you continue attacking " + ChatColor.YELLOW + attacker.getName());
                        } else {
                            attacker.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You have " + ChatColor.RED + "left combat "
                                    + ChatColor.GRAY + "with " + ChatColor.YELLOW + target.getName());
                            target.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You have " + ChatColor.RED + "left combat "
                                    + ChatColor.GRAY + "with " + ChatColor.YELLOW + attacker.getName());
                        }
                        tracking.get(attacker).remove(target);
                        cooldown.put(attacker, target);

                        BukkitRunnable cooler = new BukkitRunnable() {
                            @Override
                            public void run() {
                                cooldown.remove(attacker);
                            }
                        };
                        cooler.runTaskLater(Honour.getInstance(), 20*3);
                    }
                }
            };
            runnable.runTaskLater(Honour.getInstance(), 20*30);
        } else {
            List<Player> list = new ArrayList<>();
            list.add(target);
            tracking.put(attacker, list);

            if(Honour.getMode() == Honour.Mode.WAR_TORN) {
                attacker.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You have entered into combat with "
                        + ChatColor.GOLD + target.getName());
                attacker.sendMessage(Honour.PREFIX + ChatColor.GREEN + "You have gained (+50) Honour for aggression");
                HonourAPI.addHonour(attacker, 50);
                target.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You are now in combat with "
                        + ChatColor.GOLD + attacker.getName());
            } else {
                attacker.sendMessage(Honour.PREFIX + ChatColor.YELLOW + "You have entered into combat with "
                        + ChatColor.RED + target.getName());
                attacker.sendMessage(Honour.PREFIX + ChatColor.RED + "You have lost (-50) Honour for starting " +
                        "an unnecessary fight");
                HonourAPI.deductHonour(attacker, 50);
                target.sendMessage(Honour.PREFIX + ChatColor.YELLOW + "You are now in combat with "
                        + ChatColor.RED + attacker.getName());
                target.sendMessage(Honour.PREFIX + ChatColor.GREEN + "You will not lose Honour for defending yourself!");
            }

            BukkitRunnable runnable = new BukkitRunnable() {
                @Override
                public void run() {
                    cancel();
                    if(tracking.containsKey(attacker) && tracking.get(attacker).contains(target)) {
                        if(Honour.getMode() == Honour.Mode.PEACEFUL) {
                            attacker.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You have " + ChatColor.RED + "left combat, "
                                    + ChatColor.GRAY + "attacking " + ChatColor.YELLOW + target.getName() + ChatColor.GRAY
                                    + " again will make you lose further Honour");
                            target.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You have " + ChatColor.RED + "left combat, "
                                    + ChatColor.GRAY + "you will " + ChatColor.RED + ChatColor.BOLD.toString() + "lose karma "
                                    + ChatColor.GRAY + "if you continue attacking " + ChatColor.YELLOW + attacker.getName());
                        } else {
                            attacker.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You have " + ChatColor.RED + "left combat "
                                    + ChatColor.GRAY + "with " + ChatColor.YELLOW + target.getName());
                            target.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You have " + ChatColor.RED + "left combat "
                                    + ChatColor.GRAY + "with " + ChatColor.YELLOW + attacker.getName());
                        }
                        tracking.get(attacker).remove(target);
                        cooldown.put(attacker, target);

                        BukkitRunnable cooler = new BukkitRunnable() {
                            @Override
                            public void run() {
                                cooldown.remove(attacker);
                            }
                        };
                        cooler.runTaskLater(Honour.getInstance(), 20*3);
                    }
                }
            };
            runnable.runTaskLater(Honour.getInstance(), 20*30);
        }
    }

    /**
     * Removes a Player from combat with ALL players
     *
     * @param player to remove from combat
     */
    public static void removeFromCombat(Player player) {
        if(tracking.containsKey(player)) {
            tracking.remove(player);
        }

        for(Player p : tracking.keySet()) {
            if(tracking.get(p).contains(player)) {
                tracking.get(p).remove(player);
            }
        }
    }

    /**
     * Returns a List of all players a Player
     * is attacking
     *
     * @param player who is the attacker
     * @return a List of players or an empty list if none found
     */
    public static List<Player> getTargets(Player player) {
        if (!tracking.containsKey(player)) {
            return Collections.emptyList();
        } else {
            return tracking.get(player);
        }
    }

    /**
     * Returns a List of all players who
     * are attacking the stated Player
     *
     * @param player who is the victim/target
     * @return a List of players or an empty list if none found
     */
    public static List<Player> getAttackers(Player player) {
        List<Player> players = new ArrayList<>();
        for(Player p : tracking.keySet()) {
            if(tracking.get(p).contains(player)) {
                players.add(p);
            }
        }
        return players;
    }

    /**
     * Returns a List of all players who are
     * engaged in combat with a stated Player
     *
     * @param player who is engaged in combat
     * @return a List of players or an empty list if none found
     */
    public static List<Player> getCounterparts(Player player) {
        List<Player> list = new ArrayList<>();
        if(tracking.containsKey(player)) {
            list.addAll(tracking.get(player));
        }

        for(Player p : tracking.keySet()) {
            if(tracking.get(p).contains(player)) {
                list.add(p);
            }
        }
        return list;
    }

    /**
     * Checks whether a Player is engaged
     * in combat in any way
     *
     * @param player
     * @return
     */
    public static boolean isInCombat(Player player) {
        if(tracking.containsKey(player)) {
            return true;
        } else {
            for(Player p : tracking.keySet()) {
                if(tracking.get(p).contains(player)) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * Checks whether a Player is the attacker
     * out of two stated players in combat with
     * each other
     *
     * @param check to check whether they are the attacker
     * @param target to check whether they are the target
     * @return whether the check Player is the attacker
     */
    public static boolean isAttacker(Player check, Player target) {
        return tracking.containsKey(check) && tracking.get(check).contains(target);
    }

    /**
     * Checks whether a stated Player is engaged
     * in combat as an Attacker
     *
     * @param player to check whether they are attacking
     * @return whether the Player is attacking
     */
    public static boolean isAttacking(Player player) {
        return tracking.containsKey(player);
    }

    /**
     * Checks whether a Player is the target
     * out of two stated players in combat with
     * each other
     *
     * @param check to check whether they are the target
     * @param attacker to check whether they are the attacker
     * @return whether the check Player is the target
     */
    public static boolean isTarget(Player check, Player attacker) {
        if(!tracking.containsKey(attacker)) {
            return false;
        }
        return tracking.get(attacker).contains(check);
    }

    @EventHandler (priority = EventPriority.HIGHEST)
    public void onPlayerLeave(PlayerQuitEvent event) {
        if(isInCombat(event.getPlayer())) {
            removeFromCombat(event.getPlayer());
        }
    }

    @EventHandler (priority = EventPriority.HIGHEST)
    public void onPlayerDeath(PlayerDeathEvent event) {
        if(isInCombat(event.getEntity())) {
            if(Honour.getMode() == Honour.Mode.WAR_TORN) {
                event.getEntity().sendMessage(Honour.PREFIX + ChatColor.RED + "You have lost (-100) Honour for your failure in battle");
                HonourAPI.deductHonour(event.getEntity(), 100);
            }
            removeFromCombat(event.getEntity());
        }
    }

    @EventHandler (priority = EventPriority.HIGHEST)
    public void onPlayerDamage(EntityDamageByEntityEvent event) {
        if(event.getEntity() instanceof Player) {
            if(event.getDamager() instanceof Player) {
                Player player = (Player) event.getEntity();
                Player damager = (Player) event.getDamager();

                if(!getCounterparts(player).contains(damager)) {
                    Assassination.checkAssassination(event);
                    DivineProtection.checkDivineProtection(event);
                    Clumsy.checkClumsiness(event);
                    enterCombat(damager, player);
                }

                if(player.getHealth() <= event.getDamage()) {
                    if(!buffer.contains(player)) {
                        buffer.add(player);
                        if(Honour.getMode() == Honour.Mode.WAR_TORN) {
                            damager.sendMessage(Honour.PREFIX + ChatColor.GREEN + "You've gained (+100) Honour for your ability in combat");
                            HonourAPI.addHonour(damager, 100);
                        } else {
                            if(isTarget(damager, player)) {
                                damager.sendMessage(Honour.PREFIX + ChatColor.GREEN + "You've gained (+50) Honour for showing a " +
                                        "bully who is boss");
                                HonourAPI.addHonour(damager, 50);
                            } else if(isAttacking(player)) {
                                damager.sendMessage(ChatColor.GREEN + "You've gained (+50) Honour for protecting the innocent");
                                HonourAPI.addHonour(damager, 50);
                            } else if(isAttacker(damager, player)) {
                                damager.sendMessage(Honour.PREFIX + ChatColor.RED + "You lose (-100) Honour for spilling the " +
                                        "blood of an innocent");
                                HonourAPI.deductHonour(damager, 100);
                            }
                            for(Player p : getCounterparts(player)) {
                                if(!p.equals(damager)) {
                                    p.sendMessage(Honour.PREFIX + ChatColor.RED + "You lose (-50) Honour for assisting in a murder");
                                    HonourAPI.deductHonour(p, 50);
                                }
                            }
                        }
                        buffer.remove(player);
                    }
                }
            } else if (event.getDamager() instanceof Projectile) {
                Projectile projectile = (Projectile) event.getDamager();
                if(projectile.getShooter() instanceof Player) {
                    Player player = (Player) event.getEntity();
                    Player damager = (Player) projectile.getShooter();

                    if(!getCounterparts(player).contains(damager)) {
                        Assassination.checkAssassination(event);
                        DivineProtection.checkDivineProtection(event);
                        Clumsy.checkClumsiness(event);
                        enterCombat(damager, player);
                        if(Honour.getMode() == Honour.Mode.WAR_TORN) {
                            damager.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You have entered into combat with "
                                    + ChatColor.GOLD + player.getName());
                            player.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You are now in combat with "
                                    + ChatColor.GOLD + damager.getName());
                        } else {
                            damager.sendMessage(Honour.PREFIX + ChatColor.YELLOW + "You have entered into combat with "
                                    + ChatColor.RED + player.getName());
                            player.sendMessage(Honour.PREFIX + ChatColor.YELLOW + "You are now in combat with "
                                    + ChatColor.RED + damager.getName());
                            player.sendMessage(Honour.PREFIX + ChatColor.GREEN + "You will not lose Honour for defending yourself!");
                        }
                    }

                    if(player.getHealth() <= event.getDamage()) {
                        if(!buffer.contains(player)) {
                            buffer.add(player);
                            if(Honour.getMode() == Honour.Mode.WAR_TORN) {
                                damager.sendMessage(Honour.PREFIX + ChatColor.GREEN + "You've gained (+100) Honour for your ability in combat");
                                HonourAPI.addHonour(damager, 100);
                            } else {
                                if(isAttacking(player)) {
                                    damager.sendMessage(ChatColor.GREEN + "You've gained (+50) Honour for protecting the innocent");
                                    HonourAPI.addHonour(damager, 50);
                                } else if(isAttacker(damager, player)) {
                                    damager.sendMessage(Honour.PREFIX + ChatColor.RED + "You lose (-100) Honour for spilling the " +
                                            "blood of an innocent");
                                    HonourAPI.deductHonour(damager, 100);
                                } else {
                                    damager.sendMessage(Honour.PREFIX + ChatColor.GREEN + "You've gained (+50) Honour for showing a " +
                                            "bully who is boss");
                                    HonourAPI.addHonour(damager, 50);
                                }
                                for(Player p : getCounterparts(player)) {
                                    if(!p.equals(damager)) {
                                        p.sendMessage(Honour.PREFIX + ChatColor.RED + "You lose (-50) Honour for assisting in a murder");
                                        HonourAPI.deductHonour(p, 50);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}
