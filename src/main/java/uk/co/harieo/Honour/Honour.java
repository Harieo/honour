package uk.co.harieo.Honour;

import com.sun.org.apache.xpath.internal.operations.Mod;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import uk.co.harieo.Honour.commands.HonourCommand;
import uk.co.harieo.Honour.listeners.CombatTracker;
import uk.co.harieo.Honour.listeners.GainHonour;
import uk.co.harieo.Honour.listeners.PlayerJoin;
import uk.co.harieo.Honour.listeners.buffs.Agile;
import uk.co.harieo.Honour.listeners.buffs.GloriousVictory;
import uk.co.harieo.Honour.listeners.buffs.HastyEscape;
import uk.co.harieo.Honour.listeners.debuffs.AutoBan;
import uk.co.harieo.Honour.menus.HonourGUI;

import java.io.File;

public class Honour extends JavaPlugin {

    public static final char BULLET_POINT = '∙';
    public static final String PREFIX = ChatColor.AQUA + ChatColor.BOLD.toString() + "Honour" + ChatColor.GRAY + BULLET_POINT + " ";

    private static Mode mode;
    private static boolean enableBuffs;
    private static boolean enableDebuffs;
    private static boolean enableAutoBan;
    private static boolean enableJoinMessages;
    private static int banHonour;

    private static FileConfiguration playerLog;
    private static Plugin plugin;

    @Override
    public void onEnable() {

        getConfig().options().copyDefaults(true);
        saveConfig();

        mode = getConfig().getBoolean("war-torn") ? Mode.WAR_TORN : Mode.PEACEFUL;
        enableBuffs = getConfig().getBoolean("enable-buffs");
        enableDebuffs = getConfig().getBoolean("enable-debuffs");
        enableAutoBan = getConfig().getBoolean("auto-ban");
        enableJoinMessages = getConfig().getBoolean("join-messages");
        banHonour = 0 - getConfig().getInt("dishonour-until-ban");
        if(banHonour > 0 && enableAutoBan) { // Warns to prevent accidental dumb moment
            getLogger().warning("You have set the system so that if a player goes above "
                    + banHonour + ", they will be banned!");
        }

        if(enableBuffs) { // Prevents buffs being activated if the user decides they are not wanted
            Bukkit.getPluginManager().registerEvents(new Agile(), this);
            Bukkit.getPluginManager().registerEvents(new GloriousVictory(), this);
            Bukkit.getPluginManager().registerEvents(new HastyEscape(), this);
            // ASSASSINATION AND DIVINE PROTECTION BUFF ACTIVATED VIA CombatTracker //
        }
        if (enableDebuffs) { // Prevents debuffs being activated if the user decides they are not wanted
            Bukkit.getPluginManager().registerEvents(new AutoBan(), this);
        }
        Bukkit.getPluginManager().registerEvents(new PlayerJoin(), this);
        Bukkit.getPluginManager().registerEvents(new CombatTracker(), this);
        Bukkit.getPluginManager().registerEvents(new GainHonour(), this);
        Bukkit.getPluginManager().registerEvents(new HonourGUI(), this);

        Bukkit.getPluginCommand("honour").setExecutor(new HonourCommand());
        Bukkit.getPluginCommand("honor").setExecutor(new HonourCommand());

        loadPlayerConfig();
        plugin = this;

    }

    @Override
    public void onDisable() {
        File file = new File(getDataFolder(), "players.yml");
        try {
            playerLog.save(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        saveConfig();
    }

    public static Mode getMode() {
        return mode;
    }

    public static void setMode(Mode newMode) {
        if(newMode == Mode.BOTH) {
            throw new IllegalArgumentException("You cannot set the global mode to BOTH");
        }
        mode = newMode;
    }

    public static boolean allowBuffs() {
        return enableBuffs;
    }

    public static boolean allowDebuffs() {
        return enableDebuffs;
    }

    public static boolean allowAutoBan() {
        return enableAutoBan;
    }

    public static boolean allowJoinMessage() { return enableJoinMessages; }

    public static int getBanHonour() { return banHonour; }

    public static Plugin getInstance() { return plugin; }

    private void loadPlayerConfig() {

        File file = new File(getDataFolder(), "players.yml");
        if(!file.exists()) {
            file.getParentFile().mkdirs();
            saveResource("players.yml", false);
        }

        playerLog = new YamlConfiguration();
        try {
            playerLog.load(file);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static FileConfiguration getPlayerLog() {
        return playerLog;
    }

    public enum Mode { PEACEFUL, WAR_TORN, BOTH }

}
