package uk.co.harieo.Honour.menus;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import uk.co.harieo.Honour.Honour;
import uk.co.harieo.Honour.enums.Buffs;
import uk.co.harieo.Honour.enums.Debuffs;
import uk.co.harieo.Honour.utils.HonourAPI;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class HonourGUI implements Listener {

    private Inventory inventory;

    public HonourGUI() {} // For the Listener construction

    public HonourGUI(Player player) {

        Inventory inventory = Bukkit.createInventory(null, 6*9, "Honour");
        int honour = HonourAPI.getHonour(player);
        if(honour > 0) {
            for(int i = 0; i < 9; i++) {
                inventory.setItem(i, createItem(ChatColor.GREEN + ChatColor.BOLD.toString() + "Honourable",
                        Arrays.asList("", ChatColor.GRAY + "You have " + ChatColor.GREEN + honour + ChatColor.GRAY + " honour", "",
                                ChatColor.GREEN + "Keep it up!"),
                        Material.STAINED_GLASS_PANE, (short) 5));
            }
            for(int i = 45; i < 54; i++) {
                inventory.setItem(i, createItem(ChatColor.GREEN + ChatColor.BOLD.toString() + "Honourable",
                        Arrays.asList("", ChatColor.GRAY + "You have " + ChatColor.GREEN + honour + ChatColor.GRAY + " honour", "",
                                ChatColor.GREEN + "Keep it up!"),
                        Material.STAINED_GLASS_PANE, (short) 5));
            }
        } else {
            for(int i = 0; i < 9; i++) {
                inventory.setItem(i, createItem(ChatColor.RED + ChatColor.BOLD.toString() + "Dishonourable",
                        Arrays.asList("", ChatColor.GRAY + "You have " + ChatColor.RED + honour + ChatColor.GRAY + " honour", "",
                                ChatColor.RED + "Be careful of your actions..."),
                        Material.STAINED_GLASS_PANE, (short) 14));
            }
            for(int i = 45; i < 54; i++) {
                inventory.setItem(i, createItem(ChatColor.RED + ChatColor.BOLD.toString() + "Dishonourable",
                        Arrays.asList("", ChatColor.GRAY + "You have " + ChatColor.RED + honour + ChatColor.GRAY + " honour", "",
                                ChatColor.RED + "Be careful of your actions..."),
                        Material.STAINED_GLASS_PANE, (short) 14));
            }
        }

        inventory.setItem(22, createItem(ChatColor.GREEN + ChatColor.BOLD.toString() + "Buffs " + ChatColor.GRAY + ": "
                + ChatColor.RED + ChatColor.BOLD.toString() + "Debuffs",
                Arrays.asList("",
                        ChatColor.GRAY + "Buffs or Debuffs that affect you are " + ChatColor.GREEN + "Green", "",
                        Honour.getMode() == Honour.Mode.WAR_TORN ?
                                ChatColor.GREEN + "Killing " + ChatColor.GRAY + "will increase your Honour" :
                                ChatColor.RED + "Killing " + ChatColor.GRAY + "will decrease your Honour"),
                Material.IRON_SWORD, (short) 0));

        for (Buffs buff : Buffs.values()) {
            if(buff == Buffs.HONOURED && !Honour.allowJoinMessage()) {
                continue;
            }

            if (buff.getModeRequired() == Honour.getMode()) {
                inventory.setItem(buff.getSlot(), createItem(buff.hasSufficientHonour(player) ?
                                ChatColor.GREEN + ChatColor.BOLD.toString() + buff.getName() : ChatColor.RED + ChatColor.BOLD.toString()
                                + buff.getName(), buff.getDescription(), Material.STAINED_CLAY,
                        buff.hasSufficientHonour(player) ? (short) 5 : (short) 14));
            } else if (buff.getModeRequired() == Honour.Mode.BOTH) {
                inventory.setItem(buff.getSlot(), createItem(buff.hasSufficientHonour(player) ?
                                ChatColor.GREEN + ChatColor.BOLD.toString() + buff.getName() : ChatColor.RED + ChatColor.BOLD.toString()
                                + buff.getName(), buff.getDescription(), Material.STAINED_CLAY,
                        buff.hasSufficientHonour(player) ? (short) 5 : (short) 14));
            }
        }

        for (Debuffs debuff : Debuffs.values()) {
            if(debuff == Debuffs.AUTO_BAN && !Honour.allowAutoBan()) {
                continue;
            } else if (debuff == Debuffs.DISGRACED && !Honour.allowJoinMessage()) {
                continue;
            }

            if (debuff.getModeRequired() == Honour.getMode()) {
                inventory.setItem(debuff.getSlot(), createItem(debuff.hasSufficientHonour(player) ?
                                ChatColor.GREEN + ChatColor.BOLD.toString() + debuff.getName() : ChatColor.RED + ChatColor.BOLD.toString()
                                + debuff.getName(), debuff.getDescription(), Material.STAINED_CLAY,
                        debuff.hasSufficientHonour(player) ? (short) 5 : (short) 14));
            } else if (debuff.getModeRequired() == Honour.Mode.BOTH) {
                inventory.setItem(debuff.getSlot(), createItem(debuff.hasSufficientHonour(player) ?
                        ChatColor.GREEN + ChatColor.BOLD.toString() + debuff.getName() : ChatColor.RED + ChatColor.BOLD.toString()
                        + debuff.getName(), debuff.getDescription(), Material.STAINED_CLAY,
                        debuff.hasSufficientHonour(player) ? (short) 5 : (short) 14));
            }
        }

        this.inventory = inventory;

    }

    public Inventory getInventory() {
        return inventory;
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if(event.getClickedInventory() != null) {
            if (event.getClickedInventory().getName().equals("Honour") && event.getClickedInventory().getSize() == 6 * 9) {
                event.setCancelled(true);
            }
        }
    }

    private static ItemStack createItem(String name, List<String> lore, Material material, short data) {

        ItemStack item = new ItemStack(material, 1, data);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        meta.setLore(lore);
        item.setItemMeta(meta);
        return item;

    }

}
