package uk.co.harieo.Honour.enums;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import uk.co.harieo.Honour.Honour;
import uk.co.harieo.Honour.utils.HonourAPI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum Buffs {

    AGILE("Agile I", Arrays.asList("", ChatColor.GRAY + "You have a " + ChatColor.YELLOW + "5% "
            + ChatColor.GRAY + "chance to dodge a " + ChatColor.RED + "melee attack"), 200, 12, Honour.Mode.BOTH),
    AGILE2("Agile II", Arrays.asList("", ChatColor.GRAY + "You have a " + ChatColor.YELLOW + "15% "
            + ChatColor.GRAY + "chance to dodge a " + ChatColor.RED + "melee attack"), 500, 21, Honour.Mode.BOTH),
    AGILE3("Agile III", Arrays.asList("", ChatColor.GRAY + "You have a " + ChatColor.YELLOW + "25% "
            + ChatColor.GRAY + "chance to dodge a " + ChatColor.RED + "melee attack"), 1000, 30, Honour.Mode.BOTH),
    GLORIOUS_VICTORY("Glorious Victory", Arrays.asList("",
            ChatColor.GRAY + "Gain " + ChatColor.YELLOW + "10 seconds " + ChatColor.GRAY + "of "
            + ChatColor.GREEN + "Regeneration I", ChatColor.GRAY + "after killing an enemy"), 1500, 39, Honour.Mode.WAR_TORN),
    HASTY_ESCAPE("Hasty Escape", Arrays.asList("", ChatColor.YELLOW + "20 seconds " + ChatColor.GRAY + "of " + ChatColor.AQUA + "Speed II "
            + ChatColor.GRAY + "when your health", ChatColor.GRAY + "drops below " + ChatColor.RED + "4 Hearts"), 1500, 39, Honour.Mode.PEACEFUL),
    ASSASSINATION("Assassination I", Arrays.asList("", ChatColor.GRAY + "You have a " + ChatColor.YELLOW + "10% "
            + ChatColor.GRAY + "chance to give", ChatColor.GRAY + "an enemy " + ChatColor.RED + "Weakness I "
            + ChatColor.GRAY + "for " + ChatColor.GREEN + "5 seconds", ChatColor.GRAY + "when you attack first"),
            2000, 11, Honour.Mode.WAR_TORN),
    ASSASSINATION2("Assassination II", Arrays.asList("", ChatColor.GRAY + "You have a " + ChatColor.YELLOW + "25% "
            + ChatColor.GRAY + "chance to give", ChatColor.GRAY + "an enemy " + ChatColor.RED + "Weakness I "
            + ChatColor.GRAY + "for " + ChatColor.GREEN + "5 seconds", ChatColor.GRAY + "when you attack first"), 3000, 20, Honour.Mode.WAR_TORN),
    DIVINE_PROTECTION("Divine Protection I", Arrays.asList("", ChatColor.GRAY + "You have a " + ChatColor.YELLOW + "10% "
            + ChatColor.GRAY + "chance that an enemy's attack", ChatColor.GRAY + "is " + ChatColor.RED
            + "deflected " + ChatColor.GRAY + "on their first attempt"),
            2000, 11, Honour.Mode.PEACEFUL),
    DIVINE_PROTECTION2("Divine Protection II", Arrays.asList("", ChatColor.GRAY + "You have a " + ChatColor.YELLOW + "25% "
            + ChatColor.GRAY + "chance that an enemy's attack", ChatColor.GRAY + "is " + ChatColor.RED
            + "deflected " + ChatColor.GRAY + "on their first attempt"),
            3000, 20, Honour.Mode.PEACEFUL),
    HONOURED("Honoured Player", Arrays.asList("", ChatColor.GRAY + "You will receive an announcement for",
            ChatColor.GRAY + "being an " + ChatColor.DARK_GREEN + "honourable " + ChatColor.GRAY + "player"), 10000, 29, Honour.Mode.BOTH);

    private String name; // Displayed as display name on the GUI item
    private List<String> description; // Displayed as lore on the GUI item
    private int honourRequired; // Shows how much Honour is required to achieve this buff
    private int slot; // Shows where it will appear in HonourGUI
    private Honour.Mode modeRequired; // Shows which mode is required for this effect to become active

    Buffs(String name, List<String> description, int required, int GUISlot, Honour.Mode mode) {
        this.name = name;
        List<String> fullList = new ArrayList<>(); // Required to counteract ImmutableList
        fullList.addAll(description);
        fullList.add("");
        fullList.add(ChatColor.GRAY + "Requires " + ChatColor.YELLOW + "+" + required + " Honour");
        this.description = fullList;
        this.honourRequired = required;
        this.slot = GUISlot;
        this.modeRequired = mode;
    }

    public String getName() {
        return name;
    }

    public List<String> getDescription() { return description; }

    public int getHonourRequired() {
        return honourRequired;
    }

    public int getSlot() { return slot; }

    public Honour.Mode getModeRequired() { return modeRequired; }

    /**
     * Checks whether this buff applies to
     * a certain player
     *
     * @param player to check Honour of
     * @return whether this buff should be applied to the Player
     */
    public boolean hasSufficientHonour(Player player) {
        return HonourAPI.getHonour(player) >= this.honourRequired;
    }

    /**
     * Returns all Buffs that can be activated
     * with the stated amount of Honour
     *
     * @param honour as a maximum boundary
     * @return a list of applicable Buffs found
     */
    public static List<Buffs> getApplicableBuffs(int honour) {
        List<Buffs> list = new ArrayList<>();
        for(Buffs b : values()) {
            if(b.getHonourRequired() <= honour) {
                list.add(b);
            }
        }
        return list;
    }

}
