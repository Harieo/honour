package uk.co.harieo.Honour.enums;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import uk.co.harieo.Honour.Honour;
import uk.co.harieo.Honour.utils.HonourAPI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum Debuffs {

    CLUMSY("Clumsy I", Arrays.asList("", ChatColor.GRAY + "You gain a " + ChatColor.YELLOW + "10% " + ChatColor.GRAY + "chance to get",
            ChatColor.RED + "Slowness II " + ChatColor.GRAY + "for " + ChatColor.GREEN + "5 seconds " + ChatColor.GRAY + "when attacking"),
            -200, 14, Honour.Mode.BOTH),
    CLUMSY2("Clumsy II", Arrays.asList("", ChatColor.GRAY + "You gain a " + ChatColor.YELLOW + "25% " + ChatColor.GRAY + "chance to get",
            ChatColor.RED + "Slowness II " + ChatColor.GRAY + "for " + ChatColor.GREEN + "5 seconds " + ChatColor.GRAY + "when attacking"),
    -500, 23, Honour.Mode.BOTH),
    DISGRACED("Disgraced Player", Arrays.asList("", ChatColor.GRAY + "You will be announced when you join",
            ChatColor.GRAY + "as a " + ChatColor.RED + "Dishonourable " + ChatColor.GRAY + "player"),
            -1000, 32, Honour.Mode.BOTH),
    AUTO_BAN("Automatic Ban", Arrays.asList("", ChatColor.GRAY + "You will be " + ChatColor.RED + "permanently BANNED!"),
            Honour.getBanHonour(), 41, Honour.Mode.BOTH);

    private String name;
    private List<String> description;
    private int honourRequired;
    private int slot;
    private Honour.Mode modeRequired; // Shows which mode is required for this effect to become active

    Debuffs(String name, List<String> description, int required, int GUISlot, Honour.Mode mode) {
        this.name = name;
        List<String> fullList = new ArrayList<>(); // Required to counteract ImmutableList
        fullList.addAll(description);
        fullList.add("");
        fullList.add(ChatColor.GRAY + "Requires " + ChatColor.YELLOW + required + " Honour");
        this.description = fullList;
        this.honourRequired = required;
        this.slot = GUISlot;
        this.modeRequired = mode;
    }

    public String getName() {
        return name;
    }

    public List<String> getDescription() { return description; }

    public int getHonourRequired() {
        return honourRequired;
    }

    public int getSlot() { return slot; }

    public Honour.Mode getModeRequired() {
        return modeRequired;
    }

    /**
     * Checks whether this debuff applies to
     * a certain player
     *
     * @param player to check Honour of
     * @return whether this debuff should be applied to the Player
     */
    public boolean hasSufficientHonour(Player player) {
        return this.honourRequired >= HonourAPI.getHonour(player);
    }

    /**
     * Returns all Debuffs that can be activated
     * with the stated amount of Honour
     *
     * @param honour as a maximum boundary
     * @return a list of applicable Debuffs found
     */
    public static List<Debuffs> getApplicableDebuffs(int honour) {
        List<Debuffs> list = new ArrayList<>();
        for(Debuffs b : values()) {
            if(b.getHonourRequired() >= honour) {
                list.add(b);
            }
        }
        return list;
    }

}
