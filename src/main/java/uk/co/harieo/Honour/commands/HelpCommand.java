package uk.co.harieo.Honour.commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public enum HelpCommand {

    // THESE HAVE NO EFFECT ON THE COMMAND, JUST THE HELP MESSAGE //
    BLANK("/honour", "Tells you a little bit about the plugin and who made it"),
    INFO("/honour info", "Tells you information on your honour and buffs/debuffs"),
    MODE("/honour mode", "Toggles the 'war-torn' setting in config and applies it"),
    RESET("/honour reset <player>", "Resets a player's Honour to 100"),
    SET("/honour set <player> <amount>", "Sets a player's Honour to a certain amount"),
    ADD("/honour add <player> <amount>", "Gives Honour to a player"),
    TAKE("/honour take <player> <amount>", "Takes Honour from a player"),
    HELP("/honour help", "Shows this menu");

    private String usage;
    private String description;

    HelpCommand(String usage, String description) {
        this.usage = usage;
        this.description = description;
    }

    public String getUsage() {
        return usage;
    }

    public String getDescription() {
        return description;
    }

    /**
     * Shows the list of available commands to a Player
     *
     * @param player to show the command list to
     */
    public static void sendHelpMenu(Player player) {
        player.sendMessage(ChatColor.AQUA + ChatColor.STRIKETHROUGH.toString() + "---------------------------------------------");
        player.sendMessage(ChatColor.AQUA + ChatColor.BOLD.toString() + "Honour Help Guide");
        for(HelpCommand h : values()) {
            player.sendMessage(ChatColor.GREEN + h.getUsage() + ChatColor.GRAY + " - " + h.getDescription());
        }
        player.sendMessage(ChatColor.AQUA + ChatColor.STRIKETHROUGH.toString() + "---------------------------------------------");
    }

}
