package uk.co.harieo.Honour.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import uk.co.harieo.Honour.Honour;
import uk.co.harieo.Honour.menus.HonourGUI;
import uk.co.harieo.Honour.utils.HonourAPI;

import java.util.UUID;

public class HonourCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String arg1, String[] args) {

        if(args.length == 0) {
            sender.sendMessage(Honour.PREFIX + ChatColor.GRAY + "v" + Honour.getInstance().getDescription().getVersion()
                    + " by " + ChatColor.GOLD + "Harieo");
            TextComponent component = new TextComponent("Available on SpigotMC");
            component.setColor(ChatColor.YELLOW);
            component.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://www.spigotmc.org/members/harieo.113830/"));
            TextComponent extra = new TextComponent(" (Click to View)");
            extra.setColor(ChatColor.GRAY);
            extra.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://www.spigotmc.org/members/harieo.113830/"));
            component.addExtra(extra);
            // TODO: Set the above to project page
            if(sender instanceof Player) {
                Player player = (Player) sender;
                player.spigot().sendMessage(component);
            } else {
                sender.sendMessage("Find the page here: https://www.spigotmc.org/members/harieo.113830/");
            }
            sender.sendMessage(ChatColor.GRAY + "You can use " + ChatColor.YELLOW + "/honour help "
                    + ChatColor.GRAY + "to see the command list");
            sender.sendMessage("");
            if(sender instanceof Player) {
                Player player = (Player) sender;
                player.sendMessage(Honour.PREFIX + ChatColor.GRAY + "You currently have "
                        + ChatColor.YELLOW + HonourAPI.getHonour(player) + " Honour");
            }
            return false;
        } else if (args.length == 1) {
            String argument = args[0];
            if(argument.equalsIgnoreCase("help")) {
                if(sender instanceof Player) {
                    Player player = (Player) sender;
                    HelpCommand.sendHelpMenu(player);
                    return false;
                } else {
                    sender.sendMessage("This menu can only be viewed by players!");
                    return false;
                }
            } else if(argument.equalsIgnoreCase("info")) {
                if(sender instanceof Player) {
                    Player player = (Player) sender;
                    player.openInventory(new HonourGUI(player).getInventory());
                    return false;
                } else {
                    sender.sendMessage("This menu can only be viewed by players!");
                    return false;
                }
            } else if(argument.equalsIgnoreCase("mode")) {
                if(sender instanceof Player) {
                    Player player = (Player) sender;
                    if(!player.hasPermission("honour.admin.mode")) {
                        sender.sendMessage(Honour.PREFIX + ChatColor.RED + "You do not have permission to do that!");
                        return false;
                    }
                }
                if(Honour.getMode() == Honour.Mode.WAR_TORN) {
                    Bukkit.broadcastMessage(Honour.PREFIX + ChatColor.GRAY + "The server masters decree that "
                            + ChatColor.RED + "violence is unacceptable");
                    Honour.setMode(Honour.Mode.PEACEFUL);
                    Honour.getInstance().getConfig().set("war-torn", false);
                } else {
                    Bukkit.broadcastMessage(Honour.PREFIX + ChatColor.GRAY + "The server masters decree that "
                            + ChatColor.GREEN + "violence is acceptable");
                    Honour.setMode(Honour.Mode.WAR_TORN);
                    Honour.getInstance().getConfig().set("war-torn", true);
                }
                return false;
            } else {
                if(sender instanceof Player) {
                    Player player = (Player) sender;
                    HelpCommand.sendHelpMenu(player);
                } else {
                    sender.sendMessage("Command arguments were not recognised!");
                }
                return false;
            }
        } else if (args.length == 2) {
            String argument = args[0];
            String request = args[1];
            if(argument.equalsIgnoreCase("reset")) {
                if(!checkPermissions(sender, "honour.admin.reset")) {
                    sender.sendMessage(Honour.PREFIX + ChatColor.RED + "You do not have permission to do this!");
                    return false;
                } else if(Bukkit.getPlayer(request) != null) {
                    Player target = Bukkit.getPlayer(request);
                    HonourAPI.setHonour(target, 100);
                    sender.sendMessage(Honour.PREFIX + ChatColor.GREEN + "Successfully reset their Honour to 100!");
                    target.sendMessage(Honour.PREFIX + ChatColor.YELLOW + "Your Honour has been reset to 100 by " + sender.getName());
                    return false;
                } else if(HonourAPI.isPlayerLogged(UUID.fromString(request))) {
                    HonourAPI.setHonour(UUID.fromString(request), 100);
                    sender.sendMessage(Honour.PREFIX + ChatColor.GREEN + "Successfully reset their Honour to 100!");
                    if(Bukkit.getPlayer(UUID.fromString(request)) != null) {
                        Bukkit.getPlayer(UUID.fromString(request)).sendMessage(
                                Honour.PREFIX + ChatColor.YELLOW + "Your Honour has been reset to 100 by " + sender.getName());
                    }
                    return false;
                } else {
                    sender.sendMessage(ChatColor.RED + "We could not find that player or UUID!");
                    return false;
                }
            } else if(argument.equalsIgnoreCase("mode")) {
                if(sender instanceof Player) {
                    Player player = (Player) sender;
                    if(!player.hasPermission("honour.admin.mode")) {
                        sender.sendMessage(Honour.PREFIX + ChatColor.RED + "You do not have permission to do that!");
                        return false;
                    }
                }
                if(Honour.getMode() == Honour.Mode.WAR_TORN) {
                    Bukkit.broadcastMessage(Honour.PREFIX + ChatColor.GRAY + "The server masters decree that "
                            + ChatColor.RED + "violence is unacceptable");
                    Honour.setMode(Honour.Mode.PEACEFUL);
                    Honour.getInstance().getConfig().set("war-torn", false);
                } else {
                    Bukkit.broadcastMessage(Honour.PREFIX + ChatColor.GRAY + "The server masters decree that "
                            + ChatColor.GREEN + "violence is acceptable");
                    Honour.setMode(Honour.Mode.WAR_TORN);
                    Honour.getInstance().getConfig().set("war-torn", true);
                }
                return false;
            } else if(argument.equalsIgnoreCase("info")) {
                if(sender instanceof Player) {
                    Player player = (Player) sender;
                    player.openInventory(new HonourGUI(player).getInventory());
                    return false;
                } else {
                    sender.sendMessage("This menu can only be viewed by players!");
                    return false;
                }
            } else if (argument.equalsIgnoreCase("help")) {
                if(sender instanceof Player) {
                    Player player = (Player) sender;
                    HelpCommand.sendHelpMenu(player);
                } else {
                    sender.sendMessage("Command arguments were not recognised!");
                }
                return false;
            } else {
                if(sender instanceof Player) {
                    Player player = (Player) sender;
                    HelpCommand.sendHelpMenu(player);
                } else {
                    sender.sendMessage("Command arguments were not recognised!");
                }
                return false;
            }
        } else {
            String argument = args[0];
            String request = args[1];
            String amountString = args[2];
            int amount = 0;
            try {
                amount = Integer.parseInt(amountString);
            } catch (NumberFormatException e) {
                sender.sendMessage(Honour.PREFIX + ChatColor.RED + "Expected: /honour <argument> <player> <amount>");
                return false;
            }

            if(argument.equalsIgnoreCase("set")) {
                if(!checkPermissions(sender, "honour.admin.set")) {
                    sender.sendMessage(Honour.PREFIX + ChatColor.RED + "You do not have permission to do this!");
                    return false;
                } else if(Bukkit.getPlayer(request) != null) {
                    Player player = Bukkit.getPlayer(request);
                    HonourAPI.setHonour(player, amount);
                    player.sendMessage(Honour.PREFIX + ChatColor.YELLOW + "Your Honour has been set to " + amount + " by "
                            + sender.getName());
                    sender.sendMessage(Honour.PREFIX + ChatColor.GREEN + "Successfully set that player's honour!");
                    return false;
                } else if (HonourAPI.isPlayerLogged(UUID.fromString(request))) {
                    HonourAPI.setHonour(UUID.fromString(request), amount);
                    sender.sendMessage(Honour.PREFIX + ChatColor.GREEN + "Successfully set that player's honour!");
                    if(Bukkit.getPlayer(UUID.fromString(request)) != null) {
                        Player player = Bukkit.getPlayer(UUID.fromString(request));
                        player.sendMessage(Honour.PREFIX + ChatColor.YELLOW + "Your Honour has been set to " + amount + " by "
                                + sender.getName());
                    }
                    return false;
                } else {
                    sender.sendMessage(ChatColor.RED + "We could not find that player or UUID!");
                    return false;
                }
            } else if(argument.equalsIgnoreCase("mode")) {
                if(sender instanceof Player) {
                    Player player = (Player) sender;
                    if(!player.hasPermission("honour.admin.mode")) {
                        sender.sendMessage(Honour.PREFIX + ChatColor.RED + "You do not have permission to do that!");
                        return false;
                    }
                }
                if(Honour.getMode() == Honour.Mode.WAR_TORN) {
                    Bukkit.broadcastMessage(Honour.PREFIX + ChatColor.GRAY + "The server masters decree that "
                            + ChatColor.RED + "violence is unacceptable");
                    Honour.setMode(Honour.Mode.PEACEFUL);
                    Honour.getInstance().getConfig().set("war-torn", false);
                } else {
                    Bukkit.broadcastMessage(Honour.PREFIX + ChatColor.GRAY + "The server masters decree that "
                            + ChatColor.GREEN + "violence is acceptable");
                    Honour.setMode(Honour.Mode.WAR_TORN);
                    Honour.getInstance().getConfig().set("war-torn", true);
                }
                return false;
            } else if(argument.equalsIgnoreCase("info")) {
                if(sender instanceof Player) {
                    Player player = (Player) sender;
                    player.openInventory(new HonourGUI(player).getInventory());
                    return false;
                } else {
                    sender.sendMessage("This menu can only be viewed by players!");
                    return false;
                }
            } else if(argument.equalsIgnoreCase("add")) {
                if(!checkPermissions(sender, "honour.admin.add")) {
                    sender.sendMessage(Honour.PREFIX + ChatColor.RED + "You do not have permission to do this!");
                    return false;
                } else if(Bukkit.getPlayer(request) != null) {
                    Player player = Bukkit.getPlayer(request);
                    HonourAPI.addHonour(player, amount);
                    player.sendMessage(Honour.PREFIX + ChatColor.YELLOW + "You have been given " + amount + " Honour by "
                            + sender.getName());
                    sender.sendMessage(Honour.PREFIX + ChatColor.GREEN + "Successfully added to that player's honour!");
                    return false;
                } else if (HonourAPI.isPlayerLogged(UUID.fromString(request))) {
                    HonourAPI.addHonour(UUID.fromString(request), amount);
                    sender.sendMessage(Honour.PREFIX + ChatColor.GREEN + "Successfully added to that player's honour!");
                    if(Bukkit.getPlayer(UUID.fromString(request)) != null) {
                        Player player = Bukkit.getPlayer(UUID.fromString(request));
                        player.sendMessage(Honour.PREFIX + ChatColor.YELLOW + "You have been given " + amount + " Honour by "
                                + sender.getName());
                    }
                    return false;
                } else {
                    sender.sendMessage(ChatColor.RED + "We could not find that player or UUID!");
                    return false;
                }
            } else if(argument.equalsIgnoreCase("take")) {
                if(!checkPermissions(sender, "honour.admin.take")) {
                    sender.sendMessage(Honour.PREFIX + ChatColor.RED + "You do not have permission to do this!");
                    return false;
                } else if(Bukkit.getPlayer(request) != null) {
                    Player player = Bukkit.getPlayer(request);
                    HonourAPI.deductHonour(player, amount);
                    player.sendMessage(Honour.PREFIX + ChatColor.YELLOW + "You have been deducted " + amount + " Honour by "
                            + sender.getName());
                    sender.sendMessage(Honour.PREFIX + ChatColor.GREEN + "Successfully reduced that player's honour!");
                    return false;
                } else if (HonourAPI.isPlayerLogged(UUID.fromString(request))) {
                    HonourAPI.deductHonour(UUID.fromString(request), amount);
                    sender.sendMessage(Honour.PREFIX + ChatColor.GREEN + "Successfully reduced that player's honour!");
                    if(Bukkit.getPlayer(UUID.fromString(request)) != null) {
                        Player player = Bukkit.getPlayer(UUID.fromString(request));
                        player.sendMessage(Honour.PREFIX + ChatColor.YELLOW + "You have been deducted " + amount + " Honour by "
                                + sender.getName());
                    }
                    return false;
                } else {
                    sender.sendMessage(ChatColor.RED + "We could not find that player or UUID!");
                    return false;
                }
            } else if (argument.equalsIgnoreCase("help")) {
                if(sender instanceof Player) {
                    Player player = (Player) sender;
                    HelpCommand.sendHelpMenu(player);
                } else {
                    sender.sendMessage("Command arguments were not recognised!");
                }
                return false;
            } else {
                if(sender instanceof Player) {
                    Player player = (Player) sender;
                    HelpCommand.sendHelpMenu(player);
                } else {
                    sender.sendMessage("Command arguments were not recognised!");
                }
                return false;
            }
        }

    }

    /**
     * Checks whether a CommandSender is a Player and if so,
     * whether or not they have the stated permission
     *
     * @param sender sending the command
     * @param permission to check if permission is granted
     * @return whether the player may perform the action
     */
    private static boolean checkPermissions(CommandSender sender, String permission) {
        if(sender.isOp() || sender.hasPermission(permission)) {
            return true;
        } else {
            return false;
        }
    }
}
